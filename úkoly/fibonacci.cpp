#include <iostream>
using namespace std;
int main()
{
	int number, count = 1, lastNumber = 0, prelastNumber = 0;
	bool fibonacci = false;

	while (true) {
		cout << "Zadejte cislo: ";
		cin >> number;
		if (!cin) {
			cin.clear();
			cin.ignore(256, '\n');
			cout << "Cislo neni platne. Zadejte jej znovu" << endl;
			continue;
		}
		else {
			break;
		}
	}

	while (number == 0 || count <= number) {
		count += prelastNumber;
		prelastNumber = lastNumber;
		lastNumber = count;
		if (number == 0 || count == number) {
			fibonacci = true;
			break;
		}
	}
	cout << "Cislo " << number << (fibonacci ? " je cislem Fibonacciho posloupnosti" : " neni cislem Fibonacciho posloupnosti");
	return 0;
}