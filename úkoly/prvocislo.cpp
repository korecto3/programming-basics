#include <iostream>
using namespace std;
int main()
{
	int number;
	bool prvocislo = true;
	while (true) {
		cout << "Zadejte cislo: ";
		cin >> number;
		if (!cin) {
			cin.clear();
			cin.ignore(256, '\n');
			cout << "Cislo neni platne. Zadejte jej znovu" << endl;
			continue;
		}
		else {
			break;
		}
	}

	for (int i = 2; i < number; i++) {
		if(number % i == 0) {
			prvocislo = false;
			break;
		}
	}
	cout << (prvocislo ? "je prvocislo" : "neni prvocislo");
	return 0;
}