#include <iostream>
using namespace std;

int main()
{
	int height, base, row, leftEdge, rightEdge;
	cout << "Zadejte vysku pyramidy: ";
	cin >> height;
	cout << endl;
	base = (height * 2) - 1;
	for (int i = 1; i <= height; i++) { //each row
		row = (i * 2) - 1;
		leftEdge = (base - row) / 2;
		rightEdge = base - leftEdge + 1;
		for (int j = 1; j <= base; j++) { //each cell
			cout << ((j > leftEdge && j < rightEdge) ? "*" : " ");
		}
		cout << endl;
	}
	return 0;
}