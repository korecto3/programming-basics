#include <iostream>
#include <string>
#include "workspace.h"
using namespace std;

typedef long long cislo;	//starsi zpusob
using cislo1 = int;			//novy zpusob od C++ 11

enum Den { pondeli, utery, streda, ctvrtek, patek, sobota, nedele };
enum SvetovaStrana { sever, jih, vychod, zapad };
enum Pozice { programator, sekretarka, uklizecka, reditel };
enum Pohlavi { zena, muz };

struct Datum {
	unsigned short den;
	unsigned short mesic;
	int rok;
};

struct Predmet {
	std::string nazev;
	unsigned short terminyZkousek[3];
};

struct Zamestanec {
	std::string jmeno;
	std::string prijmeni;
	Datum nastup;
	int plat;
	Pozice pozice;
	Pohlavi pohlavi;
};

bool muzuJit(SvetovaStrana kam) {
	int muzuJit = sever | jih; //bitový or
	return kam & muzuJit;
	//return(kam == sever || kam == jih);
}

void vypisDen(Den den) {
	
	string nazvyDnu[7] = { "pondeli","utery", "streda","ctvrtek","patek","sobota","nedele"};
	cout << "den je " << nazvyDnu[den] << endl;
}

void vypisZamestnance(Zamestanec *pobocka, int n) {
	string nazvyPozic[4] = { "programator","sekretarka", "uklizecka", "reditel"};
	cout << "Toto jsou zamestnanci prazske pobocky:" << endl << endl;
	for (int i = 0; i < n; i++) {
		Zamestanec z = pobocka[i];
		cout << i+1 << ".\t" << z.jmeno << " " << z.prijmeni << "\t prijat" << (z.pohlavi == 0 ? "a " : " ")
			<< z.nastup.den << ". "<< z.nastup.mesic << ". " << z.nastup.rok
			<< "\t pracuje na pozici " << nazvyPozic[z.pozice]
			<< "\t s platovym ohodnocenim "<< z.plat << ",- Kc" << endl;
	}
}

void nactiZamestnance(Zamestanec *pobocka, int n) {
	for (int i = 0; i < n; i++) {
		cout << "Zadejte krestni jmeno " << i+1 << ". zamestnance: ";
		cin >> pobocka[i].jmeno;
		cout << "Zadejte prijmeni " << i+1 << ". zamestnance: ";
		cin >> pobocka[i].prijmeni;
		cout << "Zadejte rok nastupu " << i+1 << ". zamestnance: ";
		cin >> pobocka[i].nastup.rok;
		cout << "Zadejte mesic nastupu " << i+1 << ". zamestnance: ";
		cin >> pobocka[i].nastup.mesic;
		cout << "Zadejte den nastupu " << i+1 << ". zamestnance: ";
		cin >> pobocka[i].nastup.den;
		cout << "Zadejte plat " << i + 1 << ". zamestnance: ";
		cin >> pobocka[i].plat;
		cout << "Zadejte pozici " << i + 1 << ". zamestnance (programator: 0 , sekretarka: 1, uklizecka: 2, reditel: 3): ";
		unsigned short pozice;
		cin >> pozice;
		pobocka[i].pozice = (Pozice)pozice;
	}
	vypisZamestnance(pobocka, n);
}

int main()
{
	{
		Zamestanec pepa;
		pepa.jmeno = "Josef";
		pepa.prijmeni = "Novak";
		pepa.nastup = { 12,10,2018 };
		pepa.plat = 40000;
		pepa.pozice = programator;
		pepa.pohlavi = muz;

		Zamestanec jana = { "Jana","Novotna",{1,10,2019}, 35000, sekretarka, zena};
		Zamestanec honza = pepa;
		honza.jmeno = "Jan";
		honza.prijmeni = "Svoboda";
		honza.nastup = { 19,8,2020 };
		honza.plat -= 4000;

		Zamestanec pobockaPraha[3];
		pobockaPraha[0] = pepa;
		pobockaPraha[1] = jana;
		pobockaPraha[2] = honza;

		vypisZamestnance(pobockaPraha, 3);

		Zamestanec pobockaLovosice[3];
		nactiZamestnance(pobockaLovosice,3);

		return 0;
	}
	
	
	Datum nekdy; //deklarace
	Datum nyni = {25,10,2019}; //inicializace
	Datum den = { 2,1 }; //parciální inicializace
	Datum terminy[5] = { {1,1,2019}, {2,2,2019}};

	nekdy.den = 2;
	nekdy.mesic = 3;
	nekdy.rok = nyni.rok;

	cout << nyni.den << ". " << nyni.mesic << ". " << nyni.rok << endl;
	
	nekdy = nyni; //mozno kopirovat jednu strukturu do druhe (narozdil od poli)

	Predmet matematika;
	Predmet programovani = { "Programovani",{1,2,2019} };
	Predmet anglictina;
	anglictina = programovani; //prekopiruje se i staticke pole




	Den dnes = patek;
	Den vcera = ctvrtek;
	Den zitra = (Den)(dnes + 1);
	cout << patek << endl;

	vypisDen(dnes);
	vypisDen(pondeli);
	cout << endl;

	return 0;
}