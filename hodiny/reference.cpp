#include <iostream>
using namespace std;

struct Zlomek {
	int citatel;
	int jmenovatel;
};

int abs(int x) {
	if (x < 0) {
		x *= -1;
	}
	return x;
}

void vypisZlomek(Zlomek &x) {
	if (x.jmenovatel == 1) {
		cout << x.citatel << endl;
	}
	else {
		cout << x.citatel << " / " << x.jmenovatel << endl;
	}
}

int nsd(int x, int y) {
	x = abs(x);
	y = abs(y);
	while (x != y) {
		if (x > y)	{ x = x - y; }
		else		{ y = y - x;}
	}
	return x;
}

void zakladniTvar(Zlomek &x) {
	int n = nsd(x.citatel, x.jmenovatel);
	x.citatel /= n;
	x.jmenovatel /= n;
	if (x.jmenovatel < 1) {
		x.jmenovatel *= -1;
		x.citatel *= -1;
	}
}

Zlomek secti(const Zlomek &a, const Zlomek &b) {
	Zlomek x;
	x.jmenovatel = a.jmenovatel * b.jmenovatel;
	x.citatel = a.citatel * b.jmenovatel + b.citatel * a.jmenovatel;
	zakladniTvar(x);
	return x;
}

Zlomek operator + (const Zlomek &a, const Zlomek &b) {
	return secti(a, b);
}

Zlomek odecti(const Zlomek &a, const Zlomek &b) {
	Zlomek x;
	x.jmenovatel = a.jmenovatel * b.jmenovatel;
	x.citatel = a.citatel * b.jmenovatel - b.citatel * a.jmenovatel;
	zakladniTvar(x);
	return x;
}

Zlomek operator - (const Zlomek &a, const Zlomek &b) {
	return odecti(a, b);
}

Zlomek vynasob(const Zlomek &a, const Zlomek &b) {
	Zlomek x;
	x.jmenovatel = a.jmenovatel * b.jmenovatel;
	x.citatel = a.citatel * b.citatel;
	zakladniTvar(x);
	return x;
}

Zlomek operator * (const Zlomek &a, const Zlomek &b) {
	return vynasob(a, b);
}

Zlomek vydel(const Zlomek &a, const Zlomek &b) {
	Zlomek x;
	x.jmenovatel = a.jmenovatel * b.citatel;
	x.citatel = a.citatel * b.jmenovatel;
	zakladniTvar(x);
	return x;
}

Zlomek operator / (const Zlomek &a, const Zlomek &b) {
	return vydel(a, b);
}

int main()
{
	Zlomek a = { 2,3 };
	Zlomek b = { -1,6 };
	
	Zlomek c = (a + b);
	vypisZlomek(c);

	c = (a - b);
	vypisZlomek(c);

	c = (a * b);
	vypisZlomek(c);

	c = (a / b);
	vypisZlomek(c);
	
	return 0;
}
