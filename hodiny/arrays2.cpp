#include <iostream>
using namespace std;

void vypisPrvocisla(bool *a, int n) {
	for (int i = 0; i < n; i++) {
		if(a[i])
			cout << i << "  ";
	} cout << endl;
}

int main()
{
	// E R A T O S T H E N O V O   S Í T O
	int n = 500;
	bool prv[500];

	for (int i = 0; i < n; i++) { //vytvoří tabulku pro vyškrtávání
		if (i < 2) {
			prv[i] = false;
		}
		else {
			prv[i] = true;
		}
	}
	
	for (int i = 2; i*i < n; i++) {
		if (prv[i]) { //pokud je prvocislo
			for (int j = i*i; j < n; j += i) { //promaz 
				//if(j > i) //nesmaz vychozi prvocislo
				prv[j] = false;
			}
		}
	}
	vypisPrvocisla(prv, n);


	return 0;
}