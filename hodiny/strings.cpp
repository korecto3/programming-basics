#include <iostream>
#include <string>
using namespace std;

void naVelka(string &s) {
	int i = 0;
	while (s[i] != '\0') {
		if (s[i] >= 'a' && s[i] <= 'z') {
			s[i] = s[i] - 'a' + 'A';
		}
		i++;
	}
}

void zasifrujSubstituci(string &zprava, int posun) {
	int i = 0;
	int relPosun;
	if(posun < 0)
		posun = 26 - (-posun % 26);
	while (zprava[i] != '\0') {
		if (zprava[i] >= 'A' && zprava[i] <= 'Z') {
			relPosun = ((zprava[i] - 'A' + posun) % 26);
			zprava[i] = 'A' + relPosun;
		}
		else if (zprava[i] >= 'a' && zprava[i] <= 'z') {
			relPosun = ((zprava[i] - 'a' + posun) % 26);
			zprava[i] = 'a' + relPosun;
		}
		i++;
	}
}

void zasifrujSubstituci2(string &zprava, int posun) {
	int i = 0;
	//upravit posun + - 


	while (zprava[i] != '\0') {
		if (zprava[i] >= 'A' && zprava[i] <= 'Z') {
			zprava[i] = zprava[i] + posun;
		}
		else if (zprava[i] >= 'a' && zprava[i] <= 'z') {
			zprava[i] = zprava[i] + posun;
		}
		i++;
	}
}

int main()
{

	string s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 2439# abcdefghijklmnop";
	//naVelka(s);
	cout << s << endl;
	zasifrujSubstituci(s, 200);
	zasifrujSubstituci(s, -200);
	
	//zasifrujSubstituci(s,2);
	cout << s << endl;
	return 0;
}