#include <iostream>
#include <math.h>
using namespace std;

int main() {
	
	//cvičení na součet
	{ //úloha 1 nahoře
		int a, soucet = 0;
		while (true) {
			cout << "Zadejte cele cislo. Az vam to bude stacit, zadejte nulu: ";
			cin >> a;
			if (!cin) {
				cin.clear();
				cin.ignore(256, '\n');
				cout << "Cislo neni platne. Zadejte jej znovu" << endl;
				continue;
			}
			else {
				if (a == 0) {
					cout << "Vysledek souctu je " << soucet << endl;
					break;
				}
				soucet += a;
			}
		}
	}
	
	//cvičení na for
	cout << endl;
	
	{ //úloha 1

		for (int i = 10; i <= 25; i++) {
			if (i % 2 == 0)
				cout << i << endl;
		}
	}

	{ //úloha 2
		int n;
		double x, vysledek;
		cout << "Zadejte cislo, ktere chcete umocnit: ";
		cin >> x;
		cout << "Zadejte cislo v exponentu mocneni: ";
		cin >> n;
		
		if (n > 0) {
			vysledek = x;
			for (int i = 0; i < n - 1; i++) {
				vysledek *= x;
			}
		}
		else if (n == 0) {
			x = 1;
		}
		else {
			double vysledek = 1 / x;
			for (int i = 0; i < abs(n+1); i++) {
				vysledek *=  1/x;
			}
			x = vysledek;
		}
		cout << "Vysledek mocneni je "<< vysledek << endl;
	}
	{ //úloha 3
		int a;
		cout << "Zadejte cislo: ";
		cin >> a;
		cout << "Deliteli cisla " << a << " jsou cisla ";
		for (int i = 1; i <= a; i++) {
			if (a % i == 0) {
				cout << i;
				if (i + 1 < a) {
					cout << ", ";
				}
				else {
					cout << ".\n";
				}

			}
		}

	}
	return 0;
}
