#include <iostream>
using namespace std;

void vypis(double *a, int n) {
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

double skalarniSoucin(const double *a, const double *b, int n) {
	double soucin = 0;
	for (int i = 0; i < n; i++) {
		soucin += a[i] * b[i];
	}
	return soucin;
}

bool kolme(const double *a, const double *b, int n) {
	return (skalarniSoucin(a, b, n) == 0);
}

void prohod(double *a, int i, int j) {
	double k = a[i];
	a[i] = a[j];
	a[j] = k;
}

void otoc1(double *a, int n) {
	for (int i = 0; i < n - 1; i++) {
		double k = a[i];
		a[i] = a[n - 1];

		for (int j = n - 1; j > i; j--) {
			a[j] = a[j - 1];
		}
		a[i + 1] = k;
	}
}

void otoc2(double *a, int n) {
	for (int i = 0; i < (n) / 2; i++) {
		double k = a[i];
		a[i] = a[n - 1 - i];
		a[n - 1 - i] = k;
	}
}

void setridBublinkove(double *a, int n) {

	while (true) {
		bool finished = true;
		for (int i = 1; i < n; i++) {
			if (a[i] < a[i - 1]) {
				prohod(a, i, (i - 1));
				finished = false;
			}
		}
		if (finished) {
			cout << "A mame setrizeno!" << endl;
			break;
		}
		cout << endl;
		vypis(a, n);
	}
	
}



int main()
{
	{ //priklad 1 : V Ý P I S
		double pole[3] = { 1,2,3 };
		vypis(pole, 3);
	}

	{ //priklad 2 : S K A L Á R N Í   S O U Č I N
		double pole1[3] = { 1, 0, 1 };
		double pole2[3] = { 2, 3, 4 };
		double scs = skalarniSoucin(pole1, pole2, 3);
		cout << "Skalarni soucin je " << scs << endl;
	}

	{ //priklad 3 : K O L M É   V E K T O R Y
		double pole1[3] = { 1, 0, 1 };
		double pole2[3] = { 0, 3, 0 };
		double kv = kolme(pole1, pole2, 3);
		cout << "Vektory " << (kv ? "jsou kolme" : "nejsou kolme") << endl;
	}

	{//priklad 4 : P R O H O Z E N Í   D V O U   P R V K Ů
		double pole[4] = { 1,2,3,4 };
		prohod(pole, 0, 1);
		vypis(pole, 4);
	}

	{//priklad 5 : O T O Č E N Í   P O Ř A D Í   P R V K Ů   # 1
		double pole[4] = { 1,2,3,4 };
		otoc1(pole, 4);
		vypis(pole, 4);
	}

	{//priklad 6 : O T O Č E N Í   P O Ř A D Í   P R V K Ů   # 2
		double pole[4] = { 1,2,3,4 };
		otoc2(pole, 4);
		vypis(pole, 4);
	}

	{//priklad 6 : B U B L I N K O V É   T Ř Í D Ě N Í
		double pole[8] = { 8,3,2,4,5,2,11, 9};
		setridBublinkove(pole, 8);
	}
	return 0;
}
