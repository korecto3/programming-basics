#include <iostream>
using namespace std;

int mocni(double a, int b) {
	for (int i = 0; i < b-1; i++) {
		a *= a;
	}
	return a;
}

//rekurze
long long faktorial(int a) {
	if (a <= 1)
		return 1;
	return a * faktorial(a - 1);
}

int main() {
	int x;
	cout << "Zadej cislo: ";
	cin >> x;
	int a = mocni(x,2);
	int b = faktorial(x);
	cout << "Druha mocnina cisla "<< x << " je " << a << " a faktorial " << b;
	return 0;
}