#include <iostream>
#include <string>
#include <cmath>
#include <windows.h>

#pragma execution_character_set( "utf-8" )
using namespace std;

int main() {

	int i, j,repeat = true;
	string;
	SetConsoleOutputCP(65001);
	string ovoce[5] = {"jablka","hrušky","švestky","mandarinky","meruňky"};

	navesti:

	for (i = 0; i < 5; i++) {
		cout << "Snědl jsem " << ovoce[i] << ". ";
		if (i == 3 && repeat) {
			cout << "\n Dokoupil jsem zásoby \n";
			repeat = 0;
			goto navesti;
		}
		if (i < 4) {
			cout << "Zbyly mi už jen: ";
		}
		else {
			cout << "Nezbylo mi nic :(";
		}
		
		for (j = i+1; j < 5; j++) {
			cout << ovoce[j];
			if (j + 1 == 5) {
				cout << ".\n";
			}
			else {
				cout << ", ";
			}

		}
	}

	return 0;
}
