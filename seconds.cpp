#include <iostream>
using namespace std;

int main()
{
	int seconds, rem_seconds, minutes, hours, days;
	cout << "Zadejte casovy udaj v sekundach: \n";
	cin >> seconds;
	while (seconds < 0) {
		cout << "Zadejte prosim pouze kladne hodnoty: \n";
		cin >> seconds;
	}
	cout << "Zadali jste " << seconds << " sekund. To celkem dela ";
	days = seconds / (3600 * 24);
	if (days > 0) {
		cout << days << " dni, ";
		seconds -= days * (3600 * 24);
	}

	hours = seconds / 3600;
	if (hours > 0) {
		cout << hours << " hodin, ";
		seconds -= hours * 3600;
	}

	minutes = seconds / 60;
	if (minutes > 0) {
		rem_seconds = seconds % 60;
		cout << minutes << " minut";
		if (rem_seconds > 0) {
			cout << " a " << rem_seconds << " sekund.";
		}
		else {
			cout << ".";
		}
	}
	else {

		if ((hours + days) == 0) {
			cout << "pouhych " << seconds << " sekund.";
		}
	}
return 0;
}